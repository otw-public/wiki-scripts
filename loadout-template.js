function loadout_css_add(rand, width) {
	// Add styles
	var styles = `
		#loadout-container-`+rand+` > * {
			width:`+width+`px;
		}
		
		#loadout-container-`+rand+` td img{
			height:`+width*0.22+`px;
		}
		
		.loadout-container{
			display: grid;
			justify-items: start;
			align-items: start;
		}
		
		.loadout-container a {
			align-items: end;
		}
		
		.loadout-container > *{
			grid-column-start: 1;
			grid-row-start: 1;
		}
		
		.loadout-container table{
			height: 100%;
			color:white;
			text-shadow: -1px -1px 0 #000, 1px -1px 0 #000, -1px 1px 0 #000, 1px 1px 0 #000;
			border: 0px !important;
			line-height: 1!important;
			border-spacing: 0px;
			margin-top: 0 !important;
			margin-bottom: 0 !important;
		}
		
		.loadout-container table td{
			background: 0 0!important;
			padding: 0px !important;
			border: 1px solid white;
			border: none!important;
		}
		
		.loadout-container table td a p{
			margin-left: 12px !important;
			margin-bottom: 5px !important;
			color:white;
			text-shadow: -1px -1px 0 #000, 1px -1px 0 #000, -1px 1px 0 #000, 1px 1px 0 #000;
			z-index: 1;
		}
		
		.loadout-container table td a{
			text-decoration: none!important;
		}
		
		.loadout-container-inner {
			margin-right: 0 !important;
			margin-left: auto !important;
		}
		
		.miniItem {
			margin: auto;
			display: flex;
		}
		
		.miniItem img {
			height:`+width*0.08+`px !important;
			max-width:`+width*0.08+`px !important;
			margin: auto;
			display: flex;
		}
	`
	var styleSheet = document.createElement("style")
	styleSheet.innerText = styles
	document.head.appendChild(styleSheet)
}

function loadout_html(rand) {
	// Add table
	let raw_html = (`
		<div id="loadout-container-`+rand+`" class="loadout-container">
			<img src="/bg.png" class="loadout-container-inner">
			<table class="loadout-container-inner">
				<tr style="height:5%">
					<td style="width:12%"></td>
					<td style="width:12%"></td>
					<td style="width:12%"></td>
					<td style="width:12%"></td>
					<td style="width:12%"></td>
					<td style="width:12%"></td>
					<td style="width:12%"></td>
					<td style="width:12%"></td>
					<td style="width:4%"></td>
				</tr>
				<tr style="height:16%">
					<td colspan="5">
						<a class="loadout-container" target="_blank" id="item0">
							<img alt="largeItem" style="margin-left: 35px;visibility:hidden">
							<p></p>
						</a>
					</td>
					<td colspan="3">
						<table style="width:100%;height:100%">
							<tr style="height:14%"></tr>
							<tr style="height:39%">
								<td rowspan="2" style="width:12%"></td>
								<td style="width:30%">
									<a target="_blank" class="miniItem" id="item1">
										<img alt="miniItem" style="visibility:hidden">
									</a>
								</td>
								<td style="width:30%">
									<a target="_blank" class="miniItem" id="item2">
										<img alt="miniItem" style="visibility:hidden">
									</a>
								</td>
								<td rowspan="2"></td>
							</tr>
							<tr style="height:41%">
								<td>
									<a target="_blank" class="miniItem" id="item3">
										<img alt="miniItem" style="visibility:hidden">
									</a>
								</td>
								<td>
									<a target="_blank" class="miniItem" id="item4">
										<img alt="miniItem" style="visibility:hidden">
									</a>
								</td>
							</tr>
							<tr style="height:6%"></tr>
						</table>
					</td>
				</tr>
				<tr style="height:6%"><td></td></tr>
				<tr style="height:16%">
					<td colspan="5">
						<a class="loadout-container" target="_blank" id="item5">
							<img alt="largeItem" style="margin-left: 35px;visibility:hidden">
							<p></p>
						</a>
					</td>
					<td colspan="3">
						<table style="width:100%;height:100%">
							<tr style="height:14%"></tr>
							<tr style="height:39%">
								<td rowspan="2" style="width:12%"></td>
								<td style="width:30%">
									<a target="_blank" class="miniItem" id="item6">
										<img alt="miniItem" style="visibility:hidden">
									</a>
								</td>
								<td style="width:30%">
									<a target="_blank" class="miniItem" id="item7">
										<img alt="miniItem" style="visibility:hidden">
									</a>
								</td>
								<td rowspan="2"></td>
							</tr>
							<tr style="height:41%">
								<td>
									<a target="_blank" class="miniItem" id="item8">
										<img alt="miniItem" style="visibility:hidden">
									</a>
								</td>
								<td>
									<a target="_blank" class="miniItem" id="item9">
										<img alt="miniItem" style="visibility:hidden">
									</a>
								</td>
							</tr>
							<tr style="height:6%"></tr>
						</table>
					</td>
				</tr>
				<tr style="height:6%"><td></td></tr>
				<tr style="height:16%">
					<td colspan="8">
						<a class="loadout-container" target="_blank" id="item10">
							<img alt="largeItem" style="margin-left: 35px;visibility:hidden">
							<p></p>
						</a>
					</td>
				</tr>
				<tr>
					<td colspan="2" style="height:16%">
						<a target="_blank" id="item11">
							<img alt="smallItem" style="visibility:hidden">
						</a>
					</td>
					<td colspan="2">
						<a target="_blank" id="item12">
							<img alt="smallItem" style="visibility:hidden">
						</a>
					</td>
					<td colspan="2">
						<a target="_blank" id="item13">
							<img alt="smallItem" style="visibility:hidden">
						</a>
					</td>
					<td colspan="2">
						<a target="_blank" id="item14">
							<img alt="smallItem" style="visibility:hidden">
						</a>
					</td>
					<td></td>
				</tr>
				<tr>
					<td colspan="2">
						<a target="_blank" id="item15">
							<img alt="smallItem" style="visibility:hidden">
						</a>
					</td>
					<td colspan="2">
						<a target="_blank" id="item16">
							<img alt="smallItem" style="visibility:hidden">
						</a>
					</td>
					<td colspan="2">
						<a target="_blank" id="item17">
							<img alt="smallItem" style="visibility:hidden">
						</a>
					</td>
					<td colspan="2">
						<a target="_blank" id="item18">
							<img alt="smallItem" style="visibility:hidden">
						</a>
					</td>
					<td></td>
				</tr>
			</table>
		</div>
	`);
	
	return raw_html;
}

window.addEventListener("load", async () => {
	let loadouts = document.getElementsByClassName("loadout");
	
	// First step is to build the HTML elements of each loadout node
	// We also load the parameters and save them to the node
	// This step is sequential because it should be relatively fast to edit the DOM compared to the upcoming API requests
	for(let loadout_node of loadouts) {
		let params = [];
		let params_node = null;
		// Get parameters from the list after the loadout div and remove the parameters node
		if( (params_node = loadout_node.nextElementSibling).tagName == "UL" ) {
			for(let raw_param of params_node.children) {
				params.push(raw_param.innerText.split(","));
			}
			params_node.remove();
			console.log(params)
			loadout_node.params = params;
			
			// Random string for unique ids
			var rand = (Math.random() + 1).toString(36).substring(7);
			loadout_node.rand = rand;
			
			// CSS and HTML elements
			loadout_css_add(rand, 400);
			loadout_node.innerHTML = loadout_html(rand);
			
			let new_parent = document.createElement("div");
			new_parent.style.display = "flex";
			loadout_node.parentElement.prepend(new_parent);
			// If there is a preceding paragraph
			// Position on the right of the previous div
			if(loadout_node.previousElementSibling &&
			loadout_node.previousElementSibling.tagName == "P") {
				
				loadout_node.previousElementSibling.style.width = "65%";
				loadout_node.style.width = "35%";
				
				new_parent.append(loadout_node.previousElementSibling);
			}
			new_parent.append(loadout_node);
		}
	}
	
	// The HTML is now built, we can fill it up
	
	// Before that, get a list of all API queries we will need for items
	let item_urls = [];
	for(let loadout_node of loadouts) {
		for(let item_def of loadout_node.params) {
			let url = "https://census.lithafalcon.cc/get/ps2/item?";
			// If the item name/id is not empty
			if(item_def[0]) {
				// If the item is not defined by ID
				if(isNaN(item_def[0])) {
					url += "name.en="+encodeURI(item_def[0]);
				}
				else {
					url += "item_id="+item_def[0];
				}
				item_urls.push(url+"&c:limit=1");
			}
		}
	}
	item_urls = item_urls.filter((e,i) => item_urls.indexOf(e) === i); // Remove duplicate values
	
	let items_cache = await caches.open("loadout-items-cache");
	
	// Get an array of urls in the cache
	let items_cache_urls = await items_cache.keys();
	items_cache_urls = items_cache_urls.map(k => k.url);
	
	// Remove queries already present in the cache from our list
	item_urls = item_urls.filter(e => !items_cache_urls.includes(e));
	
	// Query items asynchronously using cache
	items_cache.addAll(item_urls);
	
	// Shouldnt need the same work for images as they are cached by default by the browser
	
	// Now fill up the data in the loadouts
	for(let loadout_node of loadouts) {
		let items_def = loadout_node.params;
		// For each item that have been given parameters, add the data to the HTML elements
		for(let i = 0; i < items_def.length; i++) {
			if(items_def[i][0]) {
				let a_node = document.querySelector("#"+loadout_node.children[0].id+" #item"+i);
				
				// Edit DOM asynchronously
				(async (a_node, item_def) => {
					// Make query URL
					let url = "https://census.lithafalcon.cc/get/ps2/item?";
					if(isNaN(item_def[0])) {
						url += "name.en="+encodeURI(item_def[0]);
					}
					else {
						url += "item_id="+item_def[0];
					}
					url += "&c:limit=1";
					
					// Query item data
					let item_data = await caches.match(url).then(r => r.json());
					item_data = item_data.item_list[0];
					
					// If no item found and it's not an ID, try to guess one
					if(!item_data && isNaN(item_def[0])) {
						let items_cache = await caches.open("loadout-items-cache");
						let url = "https://census.lithafalcon.cc/get/ps2/item?name.en=*"+encodeURI(item_def[0])+"*&c:limit=1";
						await items_cache.add(url);
						item_data = await items_cache.match(url).then(r => r.json());
						item_data = item_data.item_list[0];
					}
					
					
					// If the response wasnt empty
					if(item_data) {
						// Edit DOM
						// If no wiki link, try to guess one
						a_node.href = item_def[1]?item_def[1]:"https://planetside.fandom.com/wiki/"+item_data.name.en.replace(" ","_");
						a_node.title = item_data.name.en + " on PS2 Wiki";
						a_node.children[0].src = "https://census.daybreakgames.com"+item_data.image_path;
						a_node.children[0].style.visibility = "visible";
						if(a_node.children[1]) a_node.children[1].innerHTML = item_data.name.en;
					}
				})(a_node, items_def[i]) 
			}
		}
	}
});